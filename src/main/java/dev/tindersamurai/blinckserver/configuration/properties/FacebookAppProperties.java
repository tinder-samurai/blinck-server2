package dev.tindersamurai.blinckserver.configuration.properties;

import org.springframework.lang.NonNull;

public interface FacebookAppProperties extends DynamicPropertyProvider {
	@NonNull
	String getAppSecret();

	@NonNull
	String getAppId();
}
